* slice is a reference to an array
  * For every slice, there is an array created underneath.
  * It has three components. A pointer, a length and a capacity.

**Map**
  * A map is a reference to a hash table
  * All the keys and values of a map should be same type, but they don't need to be same type.
  * Use make() to initialize a map
```
    var ages map[string]int
    ages["vignesh"] = 18 // run time error
```
* Similar to slices, maps can't be compared using == operator.
