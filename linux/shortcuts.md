# VI shortcuts

- gg : To go to the top of the file
  
- gG : To delete all the lines of a file
  
- set number : To display numbers of each line

- shift + a : To go to the beggining of a line

- shift + g : To go to the end of a file

