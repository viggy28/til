* awk default split fields by using any whitespace (space, tab) separation
* awk (like grep) reads from stdin and writes to stdout
 ```console
    cat linux/sentinel.log | awk '{ print $1 }'
```

```console
    awk '{ print $1 }' linux/sentinel.log
```
* printing multiple fields at a time - This would display field1 and field2 with a whitespace between them.

```console
awk '{ print $1 " " $2 }' linux/sentinel.log
```

* adding condition using **if**
```console
awk '{if ($2 == "INFO") {print $1 " " $2}}' linux/sentinel.log
```

* You can use different field separator (FS)
```console
awk -F. '{print $2}' linux/sentinel.log |wc -l
or
awk -F. '{print $2}' linux/sentinel.log |wc -l
```

* real time example
Finding long running queries (greater than 70s) from postgres logs
```
awk -F":" '{print $5}' postgresql.log |grep " ms" |awk '{if ($1 >= 70000) {print $0}}'
```

To find the branch where you are currently in git
```
git branch | awk '{if ($1 == "*") print $2}'
```
[reference][1]

[1]: https://gregable.com/2010/09/why-you-should-know-just-little-awk.html
